# Ocean operation

Project for the ministry of the Ecology of France. It's an interactive game about the ocean using #Python (for game board recognition), #Node.js (for calculations) and #React.js for the visualisations.